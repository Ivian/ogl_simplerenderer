#pragma once

#include "RenderedObject.h"

class Circle : public RenderedObject
{  

protected:

//	void SetVertexData();

public:

#ifdef _MSC_VER   // suppport for override
	virtual void Initialize() override {};
#else
	virtual void Initialize() {};
#endif
	Circle();
	~Circle();
};
