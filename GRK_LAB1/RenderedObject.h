#pragma once
#include "Object.h"
#include "Vector4.h"
#include "Material.h"
#include "Texture.h"
#include "Color.h"

// This should be sth like "Mesh data"
class RenderedObject :	public Object
{
private: 

	unsigned int vao;			// Vertex array handle
	unsigned int vertexBuffer;	// Vertex description handle
	unsigned int normalBuffer;
	unsigned int uv0Buffer;		// uv description handle
	unsigned int colorBuffer;	// Color desc handle

	unsigned int vertexCount;	// Count of vertices

protected :

	unsigned int renderMode;

	Material* materialUsed;
	Texture* texture0;

	void CreateBuffers(
		unsigned int vertexCount, 
		Vector4* vertexPosition,
		unsigned int normalCount,
		Vector3* normal,
		unsigned int uvCount,
		Vector2* uv0,
		unsigned int colorCount,
		Color* vertexColor,
		bool stream = false);
	
public:

	inline void setMaterial(Material* material) { materialUsed = material; }
	inline Material* getMaterial() const { return materialUsed; }

	inline void setTexture(Texture* texture) { texture0 = texture; }
	inline Texture* getTexture() const { return texture0; }

	virtual void Initialize() = 0;

	void Render();

	RenderedObject();
	RenderedObject(Vector3 position);
	~RenderedObject();
};