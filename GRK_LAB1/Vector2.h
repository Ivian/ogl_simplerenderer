#pragma once
class Vector2
{
public:

	float x;
	float y;

	Vector2();
	Vector2(float x, float y);
	Vector2(float value);

	Vector2& operator+=(const Vector2& rhs)
	{
		x += rhs.x;
		y += rhs.y;

		return *this;
	}

	Vector2& operator-=(const Vector2& rhs)
	{
		x += rhs.x;
		y += rhs.y;

		return *this;
	}

	~Vector2();
};

//Pass lhs as param, to automaticly create copy of it
inline Vector2 operator+(Vector2 lhs, const Vector2& rhs)
{
	lhs += rhs;
	return lhs;
};

//Pass lhs as param, to automaticly create copy of it
inline Vector2 operator-(Vector2 lhs, const Vector2& rhs)
{
	lhs -= rhs;
	return lhs;
};

