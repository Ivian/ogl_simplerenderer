#pragma once
#include "Vector3.h"
#include "Matrix.h"

struct  Vector4 //:public Vector3
{
public:

	float x;
	float y;
	float z;
	float w;

	Vector3 vec3() { return Vector3(x, y, z); }

	Vector4();
	Vector4(float x, float y);
	Vector4(float x, float y, float z);
	Vector4(float x, float y, float z,float w);
	Vector4(float value);
	~Vector4();
};

inline Vector4 operator*(Matrix4x4 m, Vector4 v)
{
	v.x = v.x * m[0] + v.y * m[4] + v.z * m[8] + v.w * m[12];
	v.y = v.x * m[1] + v.y * m[5] + v.z * m[9] + v.w * m[13];
	v.z = v.x * m[2] + v.y * m[6] + v.z * m[10] + v.w * m[14];
	v.w = v.x * m[3] + v.y * m[7] + v.z * m[11] + v.w * m[15];

	return v;
};
