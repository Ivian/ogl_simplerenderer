#include "Vector4.h"


Vector4::Vector4()
{
	x = 0.0f;
	y = 0.0f;
	z = 0.0f;
	w = 0.0f;
}

Vector4::Vector4(float x, float y) 
{
	this->x = x;
	this->y = y;
	z = 0.0f;
	w = 0.0f;
}
Vector4::Vector4(float x, float y, float z)
{
	this->x = x;
	this->y = y;
	this->z = z;
	w = 0.0f;
}
Vector4::Vector4(float x, float y, float z, float w)// : Vector3(x, y, z)
{
	this->x = x;
	this->y = y;
	this->z = z;
	this->w = w;

}
Vector4::Vector4(float value) 
{
	x = value;
	y = value;
	z = value;
	w = value;
}

Vector4::~Vector4()
{
}
