#pragma once
//#include <string>
#include <sstream>
#include <vector>

void Log(std::string message, ...);
void LogError(std::string message, ...);
std::vector<std::string> &Split(const std::string &s, char delim, std::vector<std::string> &elems);
std::vector<std::string> Split(const std::string &s, char delim);
float Deg2Rad(float deg);