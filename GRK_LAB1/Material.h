#pragma once
#include "Shader.h"
#include "Color.h"
class Material
{
	bool culling;
	bool blending;
	bool disableDepthWhenBlending;

	unsigned int cullingType;

	unsigned int programId;

	// Locations of variables in shader
	int mvpMatrixLocation;			// Combined MVP matrix
	int modelMatrixLocation;
	int projectionMatrixLocation;
	int viewMatrixLocation;
	int normalMatrixLocation;
	int texture0Location;
	int diffuseColorLocation;

	Shader* vertexShader;
	Shader* fragmentShader;

	static Material* currentMaterial;

	void SetAsCurrentInternal();

public:

	Color diffuseColor;
	std::string name;

	inline unsigned int getId() { return programId; }
	inline bool isUsingCombinedMatrix() { return mvpMatrixLocation > 0; }

	inline bool getCulling() { return culling; }
	inline void setCulling(bool value, unsigned int cullingType = GL_CCW)
	{ 
		culling = value;  
		this->cullingType = cullingType;
		if (currentMaterial != 0 && currentMaterial->getId() == programId)
		{
			SetAsUnused();
			SetAsCurrent();
		}
	}

	inline bool getBlend() { return blending; }
	inline void setBlend(bool value, bool disableDepth = true)
	{
		blending = value;
		disableDepthWhenBlending = disableDepth;
		if (currentMaterial != 0 && currentMaterial->getId() == programId)
		{
			SetAsUnused();
			SetAsCurrent();
		}
	}

	void SetAsCurrent();
	void SetAsUnused();

	void SetModelMatrix(const struct Matrix4x4& modelMatrix);
	void SetNormalMatrix(const struct Matrix4x4& normalMatrix);

	void SetTexture(unsigned int texId, class Texture* texture);
	void SetDiffuseColor();

	static inline void ReleaseMaterialUsed() 
	{
		if (currentMaterial != 0) currentMaterial->SetAsUnused(); 
		currentMaterial = 0;
	}

	Material(std::string name, Shader* vertexShader, Shader* fragmentShader);
	~Material();
};
