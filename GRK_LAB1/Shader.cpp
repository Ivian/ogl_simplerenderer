#include "Shader.h"


#ifdef _MSC_VER 
#include "glTools\glut.h"
#else
#include <GL/glew.h>
#include <GL/freeglut.h>
#endif
#include <cstdio>
#include <vector>
#include <fstream>
#include "Utils.h"

#ifndef _MSC_VER 
#define nullptr 0
#endif

Shader::Shader(std::string name, const char* textData, unsigned int shaderType)
{
	this->shaderType = shaderType;
	this->name = name;

	if (shaderType != GL_VERTEX_SHADER && shaderType != GL_FRAGMENT_SHADER)
	{
		LogError("Failed to create shader %s. Unreckognized shader type %d", name.c_str(), shaderType);
		return;
	}

	shaderId = glCreateShader(shaderType);
	glShaderSource(shaderId, 1, &textData, nullptr);
	glCompileShader(shaderId);

	GLint success = 0;
	glGetShaderiv(shaderId, GL_COMPILE_STATUS, &success);

	if (success == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &maxLength);

		// The maxLength includes the NULL character
		std::vector<GLchar> errorLog(maxLength);
		glGetShaderInfoLog(shaderId, maxLength, &maxLength, &errorLog[0]);

		std::string message = std::string(&errorLog[0]);

		LogError("Failed to compile shader %s", name.c_str());
		LogError(message);

		// Provide the infolog in whatever manor you deem best.
		glDeleteShader(shaderId); // Don't leak the shader.
		shaderId = 0;
	}
}

Shader* Shader::CreateFromFile(std::string name, std::string filename, unsigned int shaderType)
{
	std::ifstream in(filename.c_str(), std::ios::in | std::ios::binary);
	if (in)
	{
		std::string contents;
		in.seekg(0, std::ios::end);
		contents.resize(in.tellg());
		in.seekg(0, std::ios::beg);
		in.read(&contents[0], contents.size());
		in.close();

		return CreateFromText(name, contents.c_str(), shaderType);
	}
	return 0;
//	throw(errno);
}

Shader* Shader::CreateFromText(std::string name, const char* textData, unsigned int shaderType)
{
	return new Shader(name, textData, shaderType);
}

Shader::~Shader()
{
	if (shaderId != 0)
		glDeleteShader(shaderId);
}
