#version 330

in struct Vertex {
	vec2 uv0;
} vert;

uniform sampler2D texture0;

out vec4 out_Color;

void main(void)
{
	out_Color = texture(texture0, vert.uv0);
}