#version 330

uniform sampler2D texture0;
flat in vec4 vertexCol;

out vec4 out_Color;

void main(void)
{
	out_Color = texture(texture0,gl_PointCoord) * vertexCol;
}