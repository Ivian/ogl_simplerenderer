#version 330

layout(location=0) in vec4 position;
//layout(location=1) in vec3 normal;	// unused
//layout(location=2) in vec2 uv0;		// unused
layout(location=3) in vec4 vertexColor; 

//uniform mat4 MVPMatrix;				// unused

uniform mat4 ModelMatrix;
uniform mat4 ProjectionMatrix;
uniform mat4 ViewMatrix;				// unused

uniform float size;						// size of point

flat out vec4 vertexCol;

void main(void){
	//gl_Position = ProjectionMatrix * ViewMatrix * position;
	//gl_PointSize = 40;// size; // probably multiply by cam distance

	
	vec4 modelPosition = ModelMatrix * position;
	float size = 400.0;// / max(0.1,((ProjectionMatrix * ViewMatrix) * modelPosition).z);

    //starColor = smoothstep(1.0, 7.0, size) * in_Color;
	vertexCol = vertexColor;

	gl_Position = (ProjectionMatrix * (ViewMatrix * ModelMatrix)) * position;
	gl_PointSize = size;
}