#version 330

layout(location=0) in vec4 position;
//layout(location=1) in vec3 normal;
//layout(location=2) in vec2 uv0;
//layout(location=3) in vec2 vertexColor;

//uniform mat4 MVPMatrix;

uniform mat4 ModelMatrix;
uniform mat4 ProjectionMatrix;
uniform mat4 ViewMatrix;

void main(void){
    gl_Position = (ProjectionMatrix * (ViewMatrix * ModelMatrix)) * position;
}

