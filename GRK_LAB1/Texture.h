#pragma once
#include <string>
class Texture
{
	unsigned int textureId;

	Texture(unsigned int id) { textureId = id; }

public:

	inline unsigned int getId() { return textureId; }

	static Texture* CreateTextureFromTGA(std::string path);
	static Texture* CreateTextureFromTGA2(std::string path);
	~Texture();
};