#include "Vector2.h"

Vector2::Vector2(float x, float y)
{
	this->x = x;
	this->y = y;
}

Vector2::Vector2(float value)
{
	this->x = value;
	this->y = value;
}

Vector2::Vector2()
{
	this->x = 0.0f;
	this->y = 0.0f;
}


Vector2::~Vector2()
{
}
