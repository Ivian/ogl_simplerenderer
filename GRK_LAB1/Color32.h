#pragma once

#ifdef _MSC_VER 
#include <cstdint>
#endif

class Color32
{
public:
#ifdef _MSC_VER 
	std::uint8_t B, G, R, A;
	Color32(std::uint8_t r, std::uint8_t g, std::uint8_t b, std::uint8_t a = 255)
	{
		B = b;
		R = r;
		G = g;
		A = a;
	}

	#else
	unsigned char B,G,R,A;
	Color32(unsigned char r, unsigned char g, unsigned char b, unsigned char a = 255)
	{
		B = b;
		R = r;
		G = g;
		A = a;
	}
	#endif

	Color32()
	{
		B = R = G = 0;
		A = 255;
	}

	~Color32() {}
};
