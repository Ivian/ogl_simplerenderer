#pragma once
#include "RenderedObject.h"
#include "Color.h"

class PointSprite : public RenderedObject
{
	Color color;
	Vector4 point;// linux-specific (0.0f,0.0f,0.0f,1.0f)

#ifdef _MSC_VER   // suppport for override
	void Initialize() override;
#else
	void Initialize();
#endif

public:

	inline void setColor(Color c) { color = c; };

	PointSprite(Vector3 position = Vector3());
	~PointSprite();
};
