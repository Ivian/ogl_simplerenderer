#include "Mesh.h"
#include <vector>
#include <fstream>
#include <cstdlib>
#include "Utils.h"

Mesh* Mesh::CreateFromFile(std::string path)
{
	std::vector<unsigned int> vertexIndices, uvIndices, normalIndices;
	std::vector<Vector4> tempVertices;
	std::vector<Vector2> tempUvs;
	std::vector<Vector3> tempNormals;

	std::vector<std::string> lines;

	std::ifstream in(path.c_str(), std::ios::in);

	Mesh* mesh = new Mesh();

	std::string line;
	while (std::getline(in, line))
	{
		lines.push_back(line);
	}

	for (unsigned int i = 0; i < lines.size(); i++)
	{
		line = lines[i];
		if (line[0] == '#') continue;
		if (line[0] == 'o') mesh->name = line.substr(2, line.size());
		if (line.substr(0, 2) == "v ") // vertex
		{
			Vector4 vertex;
			
			std::vector<std::string> splittedString = Split(line, ' ');

			vertex.x = static_cast<float>(::atof(splittedString[1].c_str()));
			vertex.y = static_cast<float>(::atof(splittedString[2].c_str()));
			vertex.z = static_cast<float>(::atof(splittedString[3].c_str()));
			vertex.w = 1.0f;

			tempVertices.push_back(vertex);

			//Log("Vertex: %f %f %f", vertex.x, vertex.y, vertex.z);
		}
		else if (line.substr(0, 2) == "vt")
		{
			Vector3 uv;

			std::vector<std::string> splittedString = Split(line, ' ');

			uv.x = static_cast<float>(::atof(splittedString[1].c_str()));
			uv.y = static_cast<float>(::atof(splittedString[2].c_str()));

			tempUvs.push_back(uv);

			//Log("Uv: %f %f", uv.x, uv.y);
		}
		else if (line.substr(0, 2) == "vn")
		{
			Vector3 normal;

			std::vector<std::string> splittedString = Split(line, ' ');

			normal.x = static_cast<float>(::atof(splittedString[1].c_str()));
			normal.y = static_cast<float>(::atof(splittedString[2].c_str()));
			normal.z = static_cast<float>(::atof(splittedString[3].c_str()));

			tempNormals.push_back(normal);

			//Log("Normal: %f %f %f", normal.x, normal.y, normal.z);
		}
		else if (line.substr(0, 2) == "f ")
		{
			std::vector<std::string> splittedLine = Split(line, ' ');
			std::vector<std::string> splittedVertex1 = Split(splittedLine[1], '/');
			std::vector<std::string> splittedVertex2 = Split(splittedLine[2], '/');
			std::vector<std::string> splittedVertex3 = Split(splittedLine[3], '/');

			vertexIndices.push_back(atoi(splittedVertex1[0].c_str()));
			vertexIndices.push_back(atoi(splittedVertex2[0].c_str()));
			vertexIndices.push_back(atoi(splittedVertex3[0].c_str()));
			uvIndices.push_back(atoi(splittedVertex1[1].c_str()));
			uvIndices.push_back(atoi(splittedVertex2[1].c_str()));
			uvIndices.push_back(atoi(splittedVertex3[1].c_str()));
			normalIndices.push_back(atoi(splittedVertex1[2].c_str()));
			normalIndices.push_back(atoi(splittedVertex2[2].c_str()));
			normalIndices.push_back(atoi(splittedVertex3[2].c_str()));

		/*	Log("IND: %d/%d/%d %d/%d/%d %d/%d/%d", vertexIndices[vertexIndices.size() - 3],
				vertexIndices[vertexIndices.size() - 2],
				vertexIndices[vertexIndices.size() - 1],
				uvIndices[uvIndices.size() - 3],
				uvIndices[uvIndices.size() - 2],
				uvIndices[uvIndices.size() - 1],
				normalIndices[normalIndices.size() - 3],
				normalIndices[normalIndices.size() - 2],
				normalIndices[normalIndices.size() - 1]);*/
		}
	}

	mesh->vertexes.reserve(tempVertices.size());
	mesh->normals.reserve(normalIndices.size());
	mesh->uvs.reserve(uvIndices.size());

	/*if (tempUvs.size() < 2)
	{
		tempUvs.push_back(Vector2());
		tempUvs.push_back(Vector2());
	}*/

	for (unsigned int i = 0; i < vertexIndices.size(); i++)
		mesh->vertexes.push_back(tempVertices[vertexIndices[i] - 1]);

	for (unsigned int i = 0; i < normalIndices.size(); i++)
		mesh->normals.push_back(tempNormals[normalIndices[i] - 1]);

	for (unsigned int i = 0; i < uvIndices.size(); i++)
		if (tempUvs.size() > 1)
			mesh->uvs.push_back(tempUvs[uvIndices[i] - 1]);
		else
			mesh->uvs.push_back(Vector2());

	Log("Loaded mesh: %s", mesh->name.c_str());

	return mesh;
}

Mesh::Mesh()
{
}


Mesh::~Mesh()
{
}
