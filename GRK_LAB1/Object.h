#pragma once

#include "Vector3.h"
#include "Matrix.h"

class Object
{
protected:

	Matrix4x4 modelMatrix;

	Vector3 position;
	Vector3 rotation;
	bool enabled;

	virtual void RebuildModelMatrix();

public:

	Vector3 getForward();
	Vector3 getRight();
	Vector3 getUp();

	inline Vector3 getPosition() { return position; }
	void setPosition(Vector3 newPosition);

	inline Vector3 getRotation() { return rotation; }
	void setRotation(Vector3 newRotation);

	void Translate(Vector3 translation);
	void Rotate(Vector3 deltaRotation);

	bool isEnabled() { return enabled; }
	void Enable() { enabled = true; }
	void Enable(bool state) { enabled = state; }
	void Disable() { enabled = false; }

	Object();
	Object(Vector3 position);
	~Object();
};