#pragma once

#include "Vector3.h"

struct Matrix4x4
{
	float field[16];

	// Return empty identity matrix
	inline static Matrix4x4 const GetIdentity() 
	{
		return Matrix4x4();
	}

	inline static Matrix4x4 const Zero()
	{
		return Matrix4x4(0);
	}

	Matrix4x4& operator*=(const Matrix4x4& rhs)
	{
		unsigned int row, column, row_offset;

		for (row = 0, row_offset = row * 4; row < 4; ++row, row_offset = row * 4)
			for (column = 0; column < 4; ++column)
				field[row_offset + column] =
					(field[row_offset + 0] * rhs.field[column + 0]) +
					(field[row_offset + 1] * rhs.field[column + 4]) +
					(field[row_offset + 2] * rhs.field[column + 8]) +
					(field[row_offset + 3] * rhs.field[column + 12]);

		return *this;
	}

	float& operator[](const int& selector)
	{
		return field[selector];
	}

	void Identity();
	void Reset();

	Vector3 getForward();
	Vector3 getRight();
	Vector3 getUp();

	void Translate(const Matrix4x4& translationMatrix);
	void Translate(const Vector3& translationVector);

	// Rotates matrix about it's pivot point
	static Matrix4x4 Rotation(Vector3 euler);	

	static Matrix4x4 YawPitchRollRotation(float yaw, float pitch, float roll);
	
	static Matrix4x4 XRotationMatrix(float xAngle);
	static Matrix4x4 YRotationMatrix(float yAngle);
	static Matrix4x4 ZRotationMatrix(float zAngle);

	Matrix4x4();
	~Matrix4x4() {}
private:
	Matrix4x4(int resetValue);
}; 

// Multiply matrices. Pass lhs as param, to automaticly create copy of it
inline Matrix4x4 operator*(Matrix4x4 lhs, const Matrix4x4& rhs)
{
	lhs *= rhs;
	return lhs;
};