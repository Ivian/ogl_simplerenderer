#include "PointSprite.h"

#ifndef _MSC_VER 
#define nullptr 0
#endif

void PointSprite::Initialize()
{
	this->CreateBuffers(1, &point, 0, nullptr, 0, nullptr,1,&color,false);
}

PointSprite::PointSprite(Vector3 position) : RenderedObject(position)
{
	point = Vector4(0.0f,0.0f,0.0f,1.0f);
	renderMode = GL_POINTS;
}

PointSprite::~PointSprite()
{
}
