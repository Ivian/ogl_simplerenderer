#include "Object.h"

Vector3 Object::getForward()
{
	return Vector3(modelMatrix.field[2], modelMatrix.field[6], modelMatrix.field[10]).Normalize(); 
}

Vector3 Object::getRight()
{
	return Vector3(modelMatrix.field[0], modelMatrix.field[4], modelMatrix.field[8]).Normalize();
}

Vector3 Object::getUp()
{
	return Vector3(modelMatrix.field[1], modelMatrix.field[5], modelMatrix.field[9]).Normalize();
}

void Object::setPosition(Vector3 newPosition)
{
	this->position = newPosition;
	RebuildModelMatrix();
}

void Object::setRotation(Vector3 newRotation)
{
	this->rotation = newRotation;
	RebuildModelMatrix();
}

void Object::Translate(Vector3 translation)
{
	position += translation;
	RebuildModelMatrix();
}

void Object::Rotate(Vector3 deltaRotation)
{
	rotation += deltaRotation;
	while (rotation.x > 360.0f)
		rotation.x -= 360.0f;
	while (rotation.y > 360.0f)
		rotation.y -= 360.0f;
	while (rotation.z > 360.0f)
		rotation.z -= 360.0f;

	while (rotation.x < 0.0f)
		rotation.x += 360.0f;
	while (rotation.y < 0.0f)
		rotation.y += 360.0f;
	while (rotation.z < 0.0f)
		rotation.z += 360.0f;

	RebuildModelMatrix();
}

void Object::RebuildModelMatrix()
{
	modelMatrix = Matrix4x4::Rotation(rotation);
	modelMatrix.Translate(position);
}

Object::Object(Vector3 position)
{
	this->position = position;

	modelMatrix = Matrix4x4();
	rotation = Vector3();
	enabled = true;

	modelMatrix.Identity();
	modelMatrix.Translate(position);
}

Object::Object()
{
	position = Vector3();
	modelMatrix = Matrix4x4();
	rotation = Vector3();
	enabled = true;

	modelMatrix.Identity();
}

Object::~Object()
{
}