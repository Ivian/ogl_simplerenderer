#pragma once
#include <string>
#ifdef _MSC_VER 
#include "glTools\glew.h"
#else
#include <GL/glew.h>
#endif

class Shader
{
	unsigned int shaderId;
	unsigned int shaderType;

	Shader();

public:

	std::string name;

	inline unsigned int getId() { return shaderId; }
	inline bool isVertex() { return shaderType == GL_VERTEX_SHADER; }
	inline bool isFragment() { return shaderType == GL_FRAGMENT_SHADER; }

	static Shader* CreateFromFile(std::string name, std::string filename, unsigned int shaderType);
	static Shader* CreateFromText(std::string name, const char* textData, unsigned int shaderType);

	~Shader();

protected:
	Shader(std::string name, const char* textData, unsigned int shaderType);
};
