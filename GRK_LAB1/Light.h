#pragma once
#include "Object.h"
#include "Color.h"
class Light : public Object
{
public:

	static Color ambient;
	Color color;

	void Update(class Material* material);

	Light();
	~Light();
};