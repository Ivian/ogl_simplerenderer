#include <string>
#include <cstdarg>
#include "Utils.h"
#include <cstdio>
#include <iostream>

void Log(std::string message, ...)
{
	message = "INFO: " + message;
	va_list argptr;
	va_start(argptr, message);
	vfprintf(stdout, message.c_str(), argptr);
	va_end(argptr);
	std::cout << std::endl;
};

void LogError(std::string message, ...)
{
	message = "ERROR: " + message;
	va_list argptr;
	va_start(argptr, message);
	vfprintf(stdout, message.c_str(), argptr);
	va_end(argptr);
	std::cout << std::endl;
};

std::vector<std::string> &Split(const std::string &s, char delim, std::vector<std::string> &elems) {
	std::stringstream ss(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
	return elems;
}


std::vector<std::string> Split(const std::string &s, char delim) {
	std::vector<std::string> elems;
	Split(s, delim, elems);
	return elems;
}

float Deg2Rad(float deg)
{
	return deg*3.14159265f / 180.f;
}
