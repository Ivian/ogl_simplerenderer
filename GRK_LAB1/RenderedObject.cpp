#include "RenderedObject.h"

#ifdef _MSC_VER 
#include "glTools\glew.h"
#else
#include <GL/glew.h>
#include <GL/freeglut.h>
#endif
#ifndef _MSC_VER 
#include <cstring>
#endif
#include "Utils.h"

#ifndef _MSC_VER 
#define nullptr 0
#endif

void RenderedObject::CreateBuffers(
	unsigned int vertexCount,
	Vector4* vertexPosition,
	unsigned int normalCount,
	Vector3* normal,
	unsigned int uvCount,
	Vector2* uv0, 
	unsigned int colorCount,
	Color* vertexColor,
	bool stream)
{
	this->vertexCount = vertexCount;
	glGenVertexArrays(1, &vao);	// generate vao (Vertex Array Object)
	glBindVertexArray(vao);

	//layout(location = 0) in vec4 position;
	glGenBuffers(1, &vertexBuffer);		// Generate vb
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	
	if (!stream)
	{
		glBufferData(GL_ARRAY_BUFFER, vertexCount * sizeof(Vector4), vertexPosition, GL_STATIC_DRAW);
	}
	else
	{
		glBufferData(GL_ARRAY_BUFFER, vertexCount * sizeof(Vector4), nullptr, GL_STATIC_DRAW);
		Vector4* vert = (Vector4*)glMapBufferRange(GL_ARRAY_BUFFER, 0, vertexCount * sizeof(Vector4), GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
		memcpy(vert, vertexPosition, vertexCount * sizeof(Vector4));
		glUnmapBuffer(GL_ARRAY_BUFFER);
	}

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vector4), nullptr); 
	glEnableVertexAttribArray(0);

	if (normalCount > 0 && normal != nullptr)
	{
		//layout(location = 1) in vec3 normal;
		glGenBuffers(1, &normalBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
		glBufferData(GL_ARRAY_BUFFER, normalCount * sizeof(Vector3), normal, GL_STATIC_DRAW);

		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vector3), nullptr);
		glEnableVertexAttribArray(1);

		Log("Normals enabled");
	}

	if (uvCount > 0 && uv0 != nullptr)
	{
		//layout(location = 2) in vec2 uv0;
		glGenBuffers(1, &uv0Buffer);
		glBindBuffer(GL_ARRAY_BUFFER, uv0Buffer);
		glBufferData(GL_ARRAY_BUFFER, uvCount * sizeof(Vector2), uv0, GL_STATIC_DRAW);

		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vector2), nullptr);
		glEnableVertexAttribArray(2);
		Log("Uv enabled");
	}

	if (colorCount > 0 && vertexColor != nullptr)
	{
		//layout(location = 3) in vec4 vertexColor;
		glGenBuffers(1, &colorBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
		glBufferData(GL_ARRAY_BUFFER, colorCount * sizeof(Color), vertexColor, GL_STATIC_DRAW);

		glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(Color), nullptr);
		glEnableVertexAttribArray(3);
		Log("Vertex color enabled");
	}
}

RenderedObject::RenderedObject()
{
#ifdef _MSC_VER 
	texture0 = nullptr;
	materialUsed = nullptr;
#else
	texture0 = 0;
	materialUsed = 0;
#endif
	renderMode = GL_TRIANGLES;
}

RenderedObject::RenderedObject(Vector3 position) : Object(position)
{
#ifdef _MSC_VER 
	texture0 = nullptr;
	materialUsed = nullptr;
#else
	texture0 = 0;
	materialUsed = 0;
#endif
	renderMode = GL_TRIANGLES;
}

void RenderedObject::Render()
{
	if (materialUsed == nullptr)
	{
		LogError("Material is null!");
		//return;
	}
	else
	{
	materialUsed->SetAsCurrent();

	materialUsed->SetModelMatrix(modelMatrix);

	materialUsed->SetTexture(0,texture0);

	materialUsed->SetDiffuseColor();
}
//	glEnable(GL_CULL_FACE);
	//glCullFace(GL_FRONT);
	//	glFrontFace(GL_CCW);
	//glEnable(GL_DEPTH_TEST);
	//glDepthMask(GL_TRUE);

	glBindVertexArray(vao);
	glDrawArrays(renderMode, 0, vertexCount);
	//glDisable(GL_DEPTH_TEST);
}

RenderedObject::~RenderedObject()
{
	/*delete[] vertexColorData;
	delete[] vertexPositionData;*/
}
