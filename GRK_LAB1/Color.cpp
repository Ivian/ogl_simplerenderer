#include "Color.h"
#include "Color32.h"

Color::Color()
{
	g = b = r = 0.0f;
	a = 1.0f;
}

Color::Color(Color32 col)
{
	r = col.R / 255.0f;
	g = col.G / 255.0f;
	b = col.B / 255.0f;
	a = col.A / 255.0f;
}

Color::Color(float r, float g, float b, float a)
{
	this->r = r; this->b = b; this->a = a; this->g = g;
}