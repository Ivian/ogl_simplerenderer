#pragma once
#include "RenderedObject.h"

class MeshRenderer : public RenderedObject
{
	class Mesh* sourceMesh;

protected:

#ifdef _MSC_VER   // suppport for override
	virtual void Initialize() override;
#else
	virtual void Initialize();
#endif

public:
	MeshRenderer(class Mesh* mesh);
	MeshRenderer(class Mesh* mesh,Vector3 position);
	~MeshRenderer();
};
