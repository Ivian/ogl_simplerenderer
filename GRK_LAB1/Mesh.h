#pragma once
#include <string>
#include <vector>
#include "Vector4.h"

class Mesh
{
public:

	std::vector<Vector4> vertexes;
	std::vector<Vector3> normals;
	std::vector<Vector2> uvs;

	std::string name;

	static Mesh* CreateFromFile(std::string path);

	Mesh();
	~Mesh();
};