#pragma once

class Color
{
public:

	float r;
	float g;
	float b;
	float a;

	Color();
	Color(class Color32 col);
	Color(float r, float g, float b, float a);
	~Color() {};
};