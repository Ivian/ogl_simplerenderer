#include "MeshRenderer.h"
#include "Mesh.h"

#ifndef _MSC_VER 
#define nullptr 0
#endif

void MeshRenderer::Initialize()
{
	// Create buffers from source mesh
	this->CreateBuffers(
		sourceMesh->vertexes.size(), 
		&sourceMesh->vertexes[0], 
		0,
		&sourceMesh->normals[0],
		sourceMesh->uvs.size(),
		&sourceMesh->uvs[0],
		0,
		nullptr);
}

MeshRenderer::MeshRenderer(Mesh* sourceMesh)
{
	this->sourceMesh = sourceMesh;
}

MeshRenderer::MeshRenderer(Mesh* sourceMesh, Vector3 position) : RenderedObject(position)
{
	this->sourceMesh = sourceMesh;
}

MeshRenderer::~MeshRenderer() {}
