#define _CRT_SECURE_NO_WARNINGS
#include "Texture.h"

#include <vector>
#include <fstream>

#include <cstring>
#include "Color32.h"
#ifdef _MSC_VER 
#include <cstdint>
#include "glTools\glew.h"
#else
#include <GL/glew.h>
#include <GL/freeglut.h>
#endif
#include "Utils.h"

#ifndef _MSC_VER 
#define nullptr 0
using namespace std;
#endif

class Tga
{
private:
	std::vector<unsigned char> Pixels;
	bool ImageCompressed;
	uint32_t width, height, size, BitsPerPixel;

public:
	Tga(const char* FilePath);
	std::vector<unsigned char> GetPixels() { return this->Pixels; }
};



struct TGAHeader
{
#ifdef _MSC_VER 
	std::uint8_t  idlength;
	std::uint8_t  colormap;
	std::uint8_t  datatype;
	std::uint8_t  colormapinfo[5];
	std::uint16_t xorigin;
	std::uint16_t yorigin;
	std::uint16_t width;
	std::uint16_t height;
	std::uint8_t  bitperpel;
	std::uint8_t  description;
#else
	uint8_t  idlength;
	uint8_t  colormap;
	uint8_t  datatype;
	uint8_t  colormapinfo[5];
	uint16_t xorigin;
	uint16_t yorigin;
	uint16_t width;
	uint16_t height;
	uint8_t  bitperpel;
	uint8_t  description;
#endif
};

Texture* Texture::CreateTextureFromTGA2(std::string path){

	TGAHeader *header;
	GLchar   *buffer;
	GLint     format, internalFormat;

	FILE* file;
	long size = -1;

	if (NULL != (file = fopen(path.c_str(), "rb")) &&
		0 == fseek(file, 0, SEEK_END) &&
		-1 != (size = ftell(file)))
	{
		rewind(file);

		if (NULL != (buffer = (GLchar*)malloc(size))){
			if (size != (long)fread(buffer, sizeof(GLchar), size, file)){
				LogError("Could not read file %s\n", path.c_str());
				free(buffer);
				fclose(file);
				return nullptr;
			}
		}
		else {
			LogError("Could not allocate %li bytes.\n", size);
			fclose(file);
			return nullptr;
		}
		fclose(file);
	}
	else{
		LogError("Could not open file %s\n", path.c_str());
		return nullptr;
	}

	// Rozmiar jest mniejszy od nag��wka
	if (size <= sizeof(TGAHeader))
	{
		LogError("Too small file '%s'\n", path.c_str());
		free(buffer);
		return nullptr;
	}

	header = (TGAHeader*)buffer;

	// sprawda si� plik TGA: nieskompresowany RGB lub RGBA obraz
	if (header->datatype != 2 || (header->bitperpel != 24 && header->bitperpel != 32))
	{
		LogError("Wrong TGA format '%s'\n", path.c_str());
		free(buffer);
		return nullptr;
	}

	// format tekstury
	format = (header->bitperpel == 24 ? GL_BGR : GL_BGRA);
	internalFormat = (format == GL_BGR ? GL_RGB8 : GL_RGBA8);


	unsigned int texId = 0;
	//glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &texId);
	glBindTexture(GL_TEXTURE_2D, texId);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, header->width, header->height, 0, format, GL_UNSIGNED_BYTE, (const GLvoid*)(buffer + sizeof(TGAHeader)+header->idlength));

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); // Linear Filtering
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); // Linear Filtering

	// zwolnienie pami�ci
	free(buffer);

	return new Texture(texId);
}
/*
Texture* Texture::CreateTextureFromTGA(std::string path)
{
	std::fstream hFile(path, std::ios::in | std::ios::binary);
	if (!hFile.is_open())
	{
#ifdef VSTUDIO 
	throw std::invalid_argument("File Not Found.");
#else 
	throw 20;
#endif 
	}

	uint8_t Header[18] = { 0 };
	std::vector<uint8_t> ImageData;
	static uint8_t DeCompressed[12] = { 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };
	static uint8_t IsCompressed[12] = { 0x0, 0x0, 0xA, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };

	hFile.read(reinterpret_cast<char*>(&Header), sizeof(Header));
	std::uint32_t BitsPerPixel;
	std::uint32_t width;
	std::uint32_t height;
	std::uint32_t size;
	bool imageCompressed;
	if (!std::memcmp(DeCompressed, &Header, sizeof(DeCompressed)))
	{
		BitsPerPixel = Header[16];
		width = Header[13] * 0xFF + Header[12];
		height = Header[15] * 0xFF + Header[14];
		size = ((width * BitsPerPixel + 31) / 32) * 4 * height;

		if ((BitsPerPixel != 24) && (BitsPerPixel != 32))
		{
			hFile.close();
			//throw std::invalid_argument("Invalid File Format. Required: 24 or 32 Bit Image.");
#ifdef VSTUDIO 
	throw std::invalid_argument("Invalid File Format. Required: 24 or 32 Bit Image.");
#else 
	throw 20;
#endif 
		}

		ImageData.resize(size);
		imageCompressed = false;
		hFile.read(reinterpret_cast<char*>(ImageData.data()), size);
	}
	else if (!std::memcmp(IsCompressed, &Header, sizeof(IsCompressed)))
	{
		BitsPerPixel = Header[16];
		width = Header[13] * 0xFF + Header[12];
		height = Header[15] * 0xFF + Header[14];
		size = ((width * BitsPerPixel + 31) / 32) * 4 * height;

		if ((BitsPerPixel != 24) && (BitsPerPixel != 32))
		{
			hFile.close();
			//throw std::invalid_argument("Invalid File Format. Required: 24 or 32 Bit Image.");
#ifdef VSTUDIO 
	throw std::invalid_argument("Invalid File Format. Required: 24 or 32 Bit Image.");
#else 
	throw 20;
#endif 
		}

		Color32 Pixel;
		int CurrentByte = 0;
		std::size_t CurrentPixel = 0;
		imageCompressed = true;
		std::uint8_t ChunkHeader = { 0 };
		int BytesPerPixel = (BitsPerPixel / 8);
		ImageData.resize(width * height * sizeof(Color32));

		do
		{
			hFile.read(reinterpret_cast<char*>(&ChunkHeader), sizeof(ChunkHeader));

			if (ChunkHeader < 128)
			{
				++ChunkHeader;
				for (int I = 0; I < ChunkHeader; ++I, ++CurrentPixel)
				{
					hFile.read(reinterpret_cast<char*>(&Pixel), BytesPerPixel);

					ImageData[CurrentByte++] = Pixel.B;
					ImageData[CurrentByte++] = Pixel.G;
					ImageData[CurrentByte++] = Pixel.R;
					if (BitsPerPixel > 24) ImageData[CurrentByte++] = Pixel.A;
				}
			}
			else
			{
				ChunkHeader -= 127;
				hFile.read(reinterpret_cast<char*>(&Pixel), BytesPerPixel);

				for (int I = 0; I < ChunkHeader; ++I, ++CurrentPixel)
				{
					ImageData[CurrentByte++] = Pixel.B;
					ImageData[CurrentByte++] = Pixel.G;
					ImageData[CurrentByte++] = Pixel.R;
					if (BitsPerPixel > 24) ImageData[CurrentByte++] = Pixel.A;
				}
			}
		} while (CurrentPixel < (width * height));
	}
	else
	{
		hFile.close();
#ifdef VSTUDIO 
	throw std::invalid_argument("Invalid File Format. Required: 24 or 32 Bit Image.");
#else 
	throw 20;
#endif 
	}

	hFile.close();

	unsigned int texId = 0;
	//glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &texId);
	glBindTexture(GL_TEXTURE_2D, texId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 128, 128, 0, GL_RGBA, GL_UNSIGNED_BYTE, ImageData.data());

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); // Linear Filtering
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); // Linear Filtering

	Texture* tex = new Texture(texId);

	ImageData.clear();

	return tex;
}
*/
Texture::~Texture()
{
	if (textureId != 0)
		glDeleteTextures(1, &textureId);
}
