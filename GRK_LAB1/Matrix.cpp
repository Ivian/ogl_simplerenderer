#include "Matrix.h"

#ifdef _MSC_VER 
#include <string>	// for memset
#include "glTools\glut.h"
#else
#include <cstring>	// for memset
#include <GL/glew.h>
#include <GL/freeglut.h>
#endif
#include "Utils.h"

Vector3 Matrix4x4::getForward()
{
	return Vector3(field[2], field[6], field[10]).Normalize();
}

Vector3 Matrix4x4::getUp()
{
	return Vector3(field[1], field[5], field[9]).Normalize();
}

Vector3 Matrix4x4::getRight()
{
	return Vector3(field[0], field[4], field[8]).Normalize();
}



Matrix4x4 Matrix4x4::XRotationMatrix(float xAngle)
{
	Matrix4x4 m;

	xAngle = Deg2Rad(xAngle);
	m.Identity();
	m[5] = cosf(xAngle);
	m[6] = sinf(xAngle);
	m[9] = -m[6];
	m[10] = m[5];

	return m;
}

Matrix4x4 Matrix4x4::YRotationMatrix(float angle)
{
	Matrix4x4 m;
	angle = Deg2Rad(angle);
	m[0] = cosf(angle);
	m[2] = sinf(angle);
	m[8] = -m[2];
	m[10] = m[0];

	return m;
}

Matrix4x4 Matrix4x4::ZRotationMatrix(float xAngle)
{
	Matrix4x4 m;

	xAngle = Deg2Rad(xAngle);
	m.Identity();
	m[0] = cosf(xAngle);
	m[1] = -sinf(xAngle);
	m[4] = -m[1];
	m[5] = m[0];

	return m;
}

Matrix4x4 Matrix4x4::YawPitchRollRotation(float yaw, float pitch, float roll)
{
	Matrix4x4 m;
	glLoadIdentity();
	glLoadMatrixf(m.field);
	glRotatef(roll, 0.0f, 0.0f, 1.0f);
	glRotatef(yaw, 0.0f, 1.0f, 1.0f);
	glRotatef(pitch, 1.0f, 0.0f, 1.0f);
	glGetFloatv(GL_MODELVIEW_MATRIX, m.field);

	return m;
	//glGetFloatv(GL_MODELVIEW_MATRIX,)
}

Matrix4x4 Matrix4x4::Rotation(Vector3 euler)
{
	Matrix4x4 m;

	

	// Convert euler angles to radians
	/*euler.x = Deg2Rad(euler.x);
	euler.y = Deg2Rad(euler.y);
	euler.z = Deg2Rad(euler.z);
	

	float cosx = cosf(euler.x);
	float sinx = sinf(euler.x);
	float cosy = cosf(euler.y);
	float siny = sinf(euler.y);
	float cosz = cosf(euler.z);
	float sinz = sinf(euler.z);*/

	/*m[0] = cosy * cosz;
	m[1] = cosx * sinz + sinx*siny*cosz;
	m[2] = sinx * sinz - cosx*siny*cosz;

	m[4] = -cosy * sinz;
	m[5] = cosx * cosz - sinx*siny*sinz;
	m[6] = sinx * cosz + cosx*siny*sinz;

	m[8] = siny;
	m[9] = -sinx * cosy;
	m[10] = cosx*cosy;*/

	/*m[0] = cosy * cosz;
	m[4] = cosx * sinz + sinx*siny*cosz;
	m[8] = sinx * sinz - cosx*siny*cosz;

	m[1] = -cosy * sinz;
	m[5] = cosx * cosz - sinx*siny*sinz;
	m[9] = sinx * cosz + cosx*siny*sinz;

	m[2] = siny;
	m[6] = -sinx * cosy;
	m[10] = cosx*cosy;*/

	return /* ZRotationMatrix(euler.x) **/ YRotationMatrix(euler.y)/* * XRotationMatrix(euler.x);*/;
}

Matrix4x4::Matrix4x4()
{
	// Reset to zero
	Reset();
	// Set as identity matrix
	Identity();
}

Matrix4x4::Matrix4x4(int resetValue)
{
	// Reset to value
	memset(field, resetValue, sizeof(float)* 16);
}

void Matrix4x4::Identity()
{
	Reset();
	field[0] = 1.0f;
	field[5] = 1.0f;
	field[10] = 1.0f;
	field[15] = 1.0f;
}

void Matrix4x4::Reset()
{
	memset(field, 0, sizeof(float)* 16);
}

void Matrix4x4::Translate(const Matrix4x4& translationMatrix)
{
	*this *= translationMatrix;
}

void Matrix4x4::Translate(const Vector3& translationVector)
{
	Matrix4x4 translationMatrix;

	translationMatrix.field[12] = translationVector.x;
	translationMatrix.field[13] = translationVector.y;
	translationMatrix.field[14] = translationVector.z;

	Translate(translationMatrix);
}
