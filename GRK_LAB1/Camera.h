#pragma once
#include "Object.h"
#include "Matrix.h"

class Camera : public Object
{
	bool ortho;
	float fov;
	float near;
	float far;

	float aspect;

	Vector3 cameraLookAt;
	Vector3 cameraUp;
	Vector3 cameraRight;

	Matrix4x4 projectionMatrix;
	//Matrix4x4 viewMatrix; // viewMatrrix is basicly modelMatrix of Camera

	void RebuildMatrices();
	void SetPerspectiveProjectionMatrix();
	void SetOrthoProjectionMatrix();
#ifdef _MSC_VER   // suppport for override
	virtual void RebuildModelMatrix() override;
#else
	virtual void RebuildModelMatrix();
#endif

public:

	static Camera* main;

	inline bool isOrtho() { return ortho; }
	inline bool isPerspective() { return !ortho; }

	inline float getFov() { return fov; }
	inline float setFov(float value) { fov = value; RebuildMatrices(); }
	inline float getNear() { return near; }
	inline float setNear(float value) { near = value; RebuildMatrices(); }
	inline float getFar() { return far; }
	inline float setFar(float value) { far = value; RebuildMatrices(); }

	inline float getAspect() { return aspect; }

	inline void setOrtho(bool value) { ortho = value; RebuildMatrices(); }

	inline Matrix4x4 const getViewMatrix() { return modelMatrix; }
	inline Matrix4x4 const getProjectionMatrix() { return projectionMatrix; }
	inline Matrix4x4 const getVPMatrix() { return modelMatrix * projectionMatrix; }

	void ResolutionChanged(float newWidth, float newHeigth);

	inline static Camera& getMain() { return *main; }

	Camera();
	~Camera();
};
