#include "Camera.h"
#include "Vector4.h"


#include "Utils.h"

#ifdef _MSC_VER 
#include "glTools\glut.h"
#else
#include <GL/glew.h>
#include <GL/freeglut.h>
#endif

void Camera::RebuildModelMatrix()
{
	cameraLookAt =  (Matrix4x4::YRotationMatrix(rotation.y) * Vector4(0.0f, 0.0f, -1.0f, 1.0f)).vec3();
//cameraLookAt = Vector3(0.0f,0.0f,-1.0f);
	cameraRight = Vector3(0.0f, 1.0f, 0.0f) * cameraLookAt;
	cameraRight.Normalize();
	cameraUp = cameraLookAt * cameraRight;
	cameraUp.Normalize();

	modelMatrix[0] = cameraRight.x;
	modelMatrix[4] = cameraRight.y;
	modelMatrix[8] = cameraRight.z;

	modelMatrix[1] = cameraUp.x;
	modelMatrix[5] = cameraUp.y;
	modelMatrix[9] = cameraUp.z;

	modelMatrix[2] = cameraLookAt.x; 
	modelMatrix[6] = cameraLookAt.y;
	modelMatrix[10] = cameraLookAt.z;

	modelMatrix[12] = -position.x;
	modelMatrix[13] = -position.y;
	modelMatrix[14] = -position.z;
	modelMatrix[15] = 1;
}

void Camera::SetPerspectiveProjectionMatrix()
{
	projectionMatrix.Reset();

	const float
		y_scale = 1.0f / (tanf((fov*M_PI / 180.0f) / 2)),
		x_scale = y_scale / aspect,
		frustum_length = far - near;

	projectionMatrix[0] = x_scale;
	projectionMatrix[5] = y_scale;
	projectionMatrix[10] = -((far + near) / frustum_length);
	projectionMatrix[11] = -1.0f;
	projectionMatrix[14] = -((2 * near * far) / frustum_length);
}

void Camera::SetOrthoProjectionMatrix()
{
	projectionMatrix.Reset();

	float size = 4.0f;
	float right = aspect * size;
	float left = -aspect * size;
	float top = size;
	float bottom = -size;

	projectionMatrix[0] = 2 / (right - left);
	projectionMatrix[5] = 2 / (top - bottom);
	projectionMatrix[10] = 2 / (far - near);

	projectionMatrix[12] = -((right + left) / (right - left));
	projectionMatrix[13] = -((top + bottom) / (top - bottom));
	projectionMatrix[14] = ((far + near) / (far - near));
	projectionMatrix[15] = 1;
}

void Camera::RebuildMatrices()
{
	if (ortho)
		SetOrthoProjectionMatrix();
	else
		SetPerspectiveProjectionMatrix();
}

void Camera::ResolutionChanged(float newWidth, float newHeigth)
{
	if (newWidth < 1.0f) newWidth = 1.0f;
	if (newHeigth < 1.0f) newHeigth = 1.0f;
	aspect = newWidth / newHeigth;
	
	RebuildMatrices();
}

Camera::Camera() : Object()
{
	ortho = false;
	fov = 60.0f;
	near = 1.0f;
	far = 30.0f;

	cameraLookAt = Vector3(0.0f, 0.0f, 1.0f);
#ifdef _MSC_VER
	if (main == nullptr) main = this;
#else
	if (main == 0) main = this;
#endif
}

Camera::~Camera()
{
}

Camera* Camera::main = 0;