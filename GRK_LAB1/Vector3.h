#pragma once
#include "Vector2.h"
#define _USE_MATH_DEFINES
#include <cmath>

struct  Vector3 : Vector2
{
public:
	float z;

	Vector3();
	Vector3(float x, float y);
	Vector3(float x, float y, float z);
	Vector3(float value);

	inline float SqrMagnitude()
	{
		return (x * x) + (y * y) + (z * z);
	}

	inline float Magnitude()
	{
		return sqrtf(SqrMagnitude());
	}

	inline Vector3 Normalized()
	{
		float _mag = Magnitude();
		return Vector3(x / _mag, y / _mag, z / _mag);
	}

	inline Vector3& Normalize()
	{
		float _mag = Magnitude();
		x /= _mag;
		y /= _mag;
		z /= _mag;
		return *this;
	}

	Vector3& operator+=(const Vector3& rhs)
	{
		x += rhs.x;
		y += rhs.y;
		z += rhs.z;

		return *this;
	}

	Vector3& operator-=(const Vector3& rhs)
	{
		x += rhs.x;
		y += rhs.y;
		z += rhs.z;

		return *this;
	}

	Vector3& operator+=(const Vector2& rhs)
	{
		x += rhs.x;
		y += rhs.y;

		return *this;
	}

	Vector3& operator-=(const Vector2& rhs)
	{
		x += rhs.x;
		y += rhs.y;

		return *this;
	}

	Vector3& operator*=(const float& rhs)
	{
		x *= rhs;
		y *= rhs;
		z *= rhs;

		return *this;
	}



	Vector3& operator/=(const float& rhs)
	{
		x /= rhs;
		y /= rhs;
		z /= rhs;

		return *this;
	}

	Vector3& operator-()
	{
		x = -x;
		y = -y;
		z = -z;

		return *this;
	}

	~Vector3();
};

//Pass lhs as param, to automaticly create copy of it
inline Vector3 operator+(Vector3 lhs, const Vector3& rhs)
{
	lhs += rhs;
	return lhs;
};

//Pass lhs as param, to automaticly create copy of it
inline Vector3 operator-(Vector3 lhs, const Vector3& rhs)
{
	lhs -= rhs;
	return lhs;
};

//Pass lhs as param, to automaticly create copy of it
inline Vector3 operator+(Vector3 lhs, const Vector2& rhs)
{
	lhs += rhs;
	return lhs;
};

//Pass lhs as param, to automaticly create copy of it
inline Vector3 operator-(Vector3 lhs, const Vector2& rhs)
{
	lhs -= rhs;
	return lhs;
};

//Pass lhs as param, to automaticly create copy of it
inline Vector3 operator*(Vector3 lhs, const float& rhs)
{
	lhs *= rhs;
	return lhs;
};

// CROSS PRODUCT
inline Vector3 operator*(const Vector3& a, const Vector3& b)
{
	Vector3 cp;
	cp.x = a.y * b.z - a.z * b.y;
	cp.y = a.z * b.x - a.x * b.z;
	cp.z = a.x * b.y - a.y * b.x;
	return cp;
};

//Pass lhs as param, to automaticly create copy of it
inline Vector3 operator/(Vector3 lhs, const float& rhs)
{
	lhs /= rhs;
	return lhs;
};