#include "Material.h"
#include "Utils.h"
#include "Matrix.h"
#include "Texture.h"
#include "Camera.h"

#include <vector>
#include "Utils.h"
Material* Material::currentMaterial = 0;

void Material::SetAsCurrent()
{
	if (currentMaterial != 0)
	{
		if (currentMaterial->getId() != programId)
		{
			currentMaterial->SetAsUnused();
			SetAsCurrentInternal();
		}
		
	}
	else
	{
		SetAsCurrentInternal();
	}
}

void Material::SetAsCurrentInternal()
{
	if (culling)
	{
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
		glFrontFace(cullingType);
	}
	if (blending)
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//glDepthMask(GL_FALSE);
		if (disableDepthWhenBlending)
			glDisable(GL_DEPTH_TEST);
	}
	glUseProgram(programId);
	currentMaterial = this;
}

void Material::SetAsUnused()
{
	if (culling)
	{
		glDisable(GL_CULL_FACE);
	}
	if (blending)
	{
		glDisable(GL_BLEND);
		//glDepthMask(GL_TRUE);
		if (disableDepthWhenBlending)
			glEnable(GL_DEPTH_TEST);
	}
	glUseProgram(0);
	currentMaterial = 0;
}

void Material::SetModelMatrix(const Matrix4x4& modelMatrix)
{
	if (isUsingCombinedMatrix())
	{

		Matrix4x4 mvp = (Camera::main->getViewMatrix() * modelMatrix);
		//Matrix4x4 proj = Camera::main->getProjectionMatrix();
		mvp *= Camera::main->getProjectionMatrix();
		//	Matrix4x4 mvp = Camera::main->getProjectionMatrix() * Camera::main->getViewMatrix() * modelMatrix;
		glUniformMatrix4fv(mvpMatrixLocation, 1, GL_FALSE, mvp.field);
	}
	else
	{
		glUniformMatrix4fv(modelMatrixLocation, 1, GL_FALSE, modelMatrix.field);
		glUniformMatrix4fv(projectionMatrixLocation, 1, GL_FALSE, Camera::main->getProjectionMatrix().field);
		glUniformMatrix4fv(viewMatrixLocation, 1, GL_FALSE, Camera::main->getViewMatrix().field);
	}
}

void Material::SetNormalMatrix(const Matrix4x4& normalMatrix)
{
	glUniformMatrix4fv(normalMatrixLocation, 1, GL_TRUE, normalMatrix.field);
}

void Material::SetTexture(unsigned int texId, Texture* texture)
{
try
{
	//Log("texture... %d",texture);
	if (texture > 0)
	{
		glBindTexture(GL_TEXTURE_2D, texture->getId());
		glUniform1i(texture0Location, 0);// texture->getId());
	}
}
catch(...)
{
	LogError("UNHANDLED EXCEPTION");
}
}

void Material::SetDiffuseColor()
{
	if (diffuseColorLocation > 0)
	{
		glUniform4f(diffuseColorLocation, diffuseColor.r , 
						diffuseColor.g, 
						diffuseColor.b , 
						diffuseColor.a );
	}
}

Material::Material(std::string name, Shader* vertexShader, Shader* fragmentShader)
{
	this->name = name;
	this->vertexShader = vertexShader;
	this->fragmentShader = fragmentShader;
	this->disableDepthWhenBlending = false;
	if (vertexShader->isVertex() == false)
	{
		LogError("Vertex shader given for material %d is not a vertex shader", name.c_str());
		return;
	}

	if (fragmentShader->isFragment() == false)
	{
		LogError("Fragment shader given for material %d is not a fragment shader", name.c_str());
		return;
	}

	programId = glCreateProgram();
	glAttachShader(programId, vertexShader->getId());
	glAttachShader(programId, fragmentShader->getId());
	glLinkProgram(programId);

	GLint isLinked = 0;
	glGetProgramiv(programId, GL_LINK_STATUS, (int *)&isLinked);
	if (isLinked == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetProgramiv(programId, GL_INFO_LOG_LENGTH, &maxLength);

		//The maxLength includes the NULL character
		std::vector<GLchar> infoLog(maxLength);
		glGetProgramInfoLog(programId, maxLength, &maxLength, &infoLog[0]);

		std::string message = std::string(&infoLog[0]);

		LogError("Failed to link shaders %s and %s into material %s", vertexShader->name.c_str(), fragmentShader->name.c_str(), name.c_str());
		LogError(message);

		glDeleteProgram(programId);
		programId = 0;

		return;
	}

	glValidateProgram(programId);

	modelMatrixLocation = glGetUniformLocation(programId, "ModelMatrix");
	projectionMatrixLocation = glGetUniformLocation(programId, "ProjectionMatrix");
	viewMatrixLocation = glGetUniformLocation(programId, "ViewMatrix");
	normalMatrixLocation = glGetUniformLocation(programId, "NormalMatrix");

	diffuseColorLocation = glGetUniformLocation(programId, "diffuseColor");
	texture0Location = glGetUniformLocation(programId, "texture0");

	mvpMatrixLocation = glGetUniformLocation(programId, "MVPMatrix");

	Log("Linked material: %s. Using MVP: %s", name.c_str(), isUsingCombinedMatrix() ? "true" : "false");
}

Material::~Material()
{
	if (programId != 0) glDeleteProgram(programId);
}
