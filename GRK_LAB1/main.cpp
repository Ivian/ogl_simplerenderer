#include <cstdio>
#include <vector>

#ifdef _MSC_VER 
#include "glTools\glew.h"
#include "glTools\glut.h"

#pragma comment (lib,"glut32.lib")
#pragma comment (lib,"glew32.lib")

#else
#include <GL/glew.h>
#include <GL/freeglut.h>
#endif


#include "Vector4.h"
#include "Material.h"

#include "Camera.h"
#include "Utils.h"
#include "Mesh.h"
#include "MeshRenderer.h"
#include "PointSprite.h"

Shader* vertexPass;
Shader* fragmentUnlit;

Shader* diffuseVert;
Shader* diffuseFrag;

Shader* colorTransparentFrag;

Shader* pointSpriteFrag;
Shader* pointSpriteVert;

Material* unlit;
Material* diffuse;
Material* lanternDiffuse;
Material* colorTransparent;
Material* pointSpriteMaterial;

Texture* lanternTexture;
Texture* groundTexture;
Texture* seaTexture;
Texture* haloTexture;

Mesh* seaMesh;
Mesh* lanternMesh;
Mesh* groundMesh;
Mesh* coneMesh;

Camera mainCamera;

std::vector<RenderedObject*> objects;
std::vector<Mesh*> meshes;

bool uparrow, downArrow, leftArrow, rigtArrow,
akey, dkey, skey, wkey;

const float cameraSpeed = 4.0f;

void SpecialKeyboardDown(int Key, int X, int Y);
void SpecialKeyboardUp(int Key, int X, int Y);
void KeyboardFunction(unsigned char Key, int X, int Y);
void KeyboardUpFunction(unsigned char Key, int X, int Y);
void ResizeWindow(int newWidth, int newHeigth);
void CreateObjects();
void Cleanup();

void display();

void Init(int windowWidth, int windowHeigth)
{
	glutInitWindowSize(windowWidth, windowHeigth);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	
	glutCreateWindow("OPEN GL");

	glutReshapeFunc(ResizeWindow);
	glutDisplayFunc(display);
	glutShowWindow();

	glutKeyboardFunc(KeyboardFunction);
	glutKeyboardUpFunc(KeyboardUpFunction);
	glutSpecialFunc(SpecialKeyboardDown);
	glutSpecialUpFunc(SpecialKeyboardUp);

	glewExperimental = GL_TRUE;
	GLenum res = glewInit();
	if (res != GLEW_OK)
	{
		LogError("OpenGL Error: %s", glewGetErrorString(res));
		return;
	}

	Log("OpenGL Version: %s", glGetString(GL_VERSION));

	mainCamera.setOrtho(false);
	ResizeWindow(windowWidth, windowHeigth);

	vertexPass = Shader::CreateFromFile("Vertex Pass", "shaders/VertexPass.vert", GL_VERTEX_SHADER);
	fragmentUnlit = Shader::CreateFromFile("Fragment Unlit", "shaders/Unlit.frag", GL_FRAGMENT_SHADER);

	unlit = new Material("Unlit", vertexPass, fragmentUnlit);

	diffuseVert = Shader::CreateFromFile("Diffuse Vertex", "shaders/Diffuse.vert", GL_VERTEX_SHADER);
	diffuseFrag = Shader::CreateFromFile("Diffuse Fragment", "shaders/Diffuse.frag", GL_FRAGMENT_SHADER);

	diffuse = new Material("Diffuse", diffuseVert, diffuseFrag);
	diffuse->setCulling(true);
	lanternDiffuse = new Material("Lantern Diffuse", diffuseVert, diffuseFrag);
	lanternDiffuse->setCulling(false, GL_CW);

	colorTransparentFrag = Shader::CreateFromFile("Color Transparent", "shaders/ColorTransparent.frag", GL_FRAGMENT_SHADER);

	colorTransparent = new Material("Color Transparent", vertexPass, colorTransparentFrag);
	colorTransparent->setCulling(true);
	colorTransparent->setBlend(true,false);
	colorTransparent->diffuseColor = Color(1.0f, 0.5f, 0.0f, 0.5f);

	pointSpriteVert = Shader::CreateFromFile("Point Sprite Vert", "shaders/PointSprite.vert", GL_VERTEX_SHADER);
	pointSpriteFrag = Shader::CreateFromFile("Point Sprite Frag", "shaders/PointSprite.frag", GL_FRAGMENT_SHADER);

	pointSpriteMaterial = new Material("Point Sprite", pointSpriteVert, pointSpriteFrag);
	pointSpriteMaterial->setBlend(true,false);

	lanternTexture = Texture::CreateTextureFromTGA2("res/Lantern.tga");
	groundTexture = Texture::CreateTextureFromTGA2("res/GroundTexture.tga");
	seaTexture = Texture::CreateTextureFromTGA2("res/Sea.tga");
	haloTexture = Texture::CreateTextureFromTGA2("res/halo.tga");
	
	groundMesh = Mesh::CreateFromFile("res/Ground.obj");
	lanternMesh = Mesh::CreateFromFile("res/Lantern.obj");
	seaMesh = Mesh::CreateFromFile("res/Sea.obj");
	coneMesh = Mesh::CreateFromFile("res/cone.obj");

	mainCamera.setPosition(Vector3(1.0f,5.0f,5.0f));

	//meshes.push_back(Mesh::CreateFromFile("res/cube.obj"));
	glClearColor(0.1, 0.1, 0.1, 1.0);

	glEnable(GL_PROGRAM_POINT_SIZE);
	glEnable(GL_POINT_SPRITE);

	glEnable(GL_DEPTH_TEST);

	GLenum ErrorValue = glGetError();

	if (ErrorValue != GL_NO_ERROR)
		LogError("CRITICAL: %s",gluErrorString(ErrorValue));

	glDepthFunc(GL_LESS);
ErrorValue = glGetError();

	if (ErrorValue != GL_NO_ERROR)
		LogError("CRITICAL: %s",gluErrorString(ErrorValue));
	glDepthMask(GL_TRUE);

}

void ResizeWindow(int newWidth, int newHeigth)
{
	glViewport(0, 0, newWidth, newHeigth);
	Camera& camera = Camera::getMain();
	camera.ResolutionChanged(newWidth, newHeigth);
}

int oldTimeSinceStart = 0;
float deltaTime = 0.0f;

// Main drawing routine. 
void display()
{
	//Log("display 0");
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //Clear the screen 
//Log("display 1");
	int timeSinceStart = glutGet(GLUT_ELAPSED_TIME);
	deltaTime = (timeSinceStart - oldTimeSinceStart) / 1000.0f;
	oldTimeSinceStart = timeSinceStart;
//Log("display 2");
	if (uparrow)
		mainCamera.Translate(Vector3(0.0f, deltaTime * cameraSpeed));
	else if (downArrow)
		mainCamera.Translate(Vector3(0.0f, -deltaTime * cameraSpeed));
	else if (leftArrow)
		mainCamera.Translate(Vector3(-deltaTime * cameraSpeed, 0.0f));
	else if (rigtArrow)
		mainCamera.Translate(Vector3(deltaTime * cameraSpeed, 0.0f));
//Log("display 3");
	objects[3]->Rotate(Vector3(0.0f, 20.0f * deltaTime));

	if (akey)
		mainCamera.Rotate(Vector3(0.0f, 100.0f * deltaTime));
	else if (dkey)
		mainCamera.Rotate(Vector3(0.0f, 100.0f * -deltaTime));
	//Log("display 4");

	for (unsigned int i = 0; i < objects.size(); i++)
		objects[i]->Render();
//Log("display 5");
	glutSwapBuffers();
	glutPostRedisplay();
//Log("display 6");
}


int main(int argc, char** argv)
{
	glutInit(&argc, argv);

	Init(800, 600);



	CreateObjects();

	//Log("after init");

	glutMainLoop();

	//Log("quit -s");

	return 0;
}

void CreateObjects()
{
	MeshRenderer* ground = new MeshRenderer(groundMesh);
	ground->setTexture(groundTexture);
	ground->setMaterial(diffuse);
	objects.push_back(ground);

	MeshRenderer* sea = new MeshRenderer(seaMesh);
	sea->setTexture(seaTexture);
	sea->setMaterial(diffuse);
	objects.push_back(sea);

	MeshRenderer* lantern = new MeshRenderer(lanternMesh);
	lantern->setTexture(lanternTexture);
	lantern->setMaterial(lanternDiffuse);
	objects.push_back(lantern);

	MeshRenderer* cone = new MeshRenderer(coneMesh);
	cone->setMaterial(colorTransparent);
	cone->Translate(Vector3(-2.4f, 4.0f, 1.5f));
	objects.push_back(cone);

	// create 4 cubes
/*	for (int i = 0; i < 4; i++)
	{
		MeshRenderer* mr = new MeshRenderer(meshes[0]);// , Vector3(0.0f, 0.0f, 5.0f));
		//mr->setTexture(testTexture);
		mr->setMaterial(unlit);

		if (i == 0)
		{
			mr->Translate(Vector3(0.0f, 0.0f, 3.0f));
			//mr->setMaterial(pointSpriteMaterial);
		}
		if (i == 1)
			mr->Translate(Vector3(3.0f, 0.0f, 0.0f));
		if ( i== 2)
			mr->Translate(Vector3(-3.0f, 0.0f, 0.0f));
		if ( i== 3)
			mr->Translate(Vector3(0.0f, 0.0f, -3.0f));

		objects.push_back(mr);
	}*/

	PointSprite* halo = new PointSprite();
	halo->setMaterial(pointSpriteMaterial);
	halo->setColor(Color(0.8f, 0.5f, 0.1f, 0.7f));
	halo->Translate(Vector3(-2.4f, 4.0f, 1.5f));
	halo->setTexture(haloTexture);
	objects.push_back(halo);

	for (unsigned int i = 0; i < objects.size(); i++)
		objects[i]->Initialize();

	//for (unsigned int i = 0; i < objects.size(); i++)
	//	objects[i]->setMaterial(unlit);
}

void KeyboardFunction(unsigned char Key, int X, int Y)
{
	//Log("KEY DOWN %d", Key);
	switch (Key){
	case 27:
		//glutLeaveMainLoop();
		break;
	case 97:		// a
		akey = true;
		break;
	case 100:		// d
		dkey = true;
		break;
	default:

		break;
	}

	glutPostRedisplay();
}

void KeyboardUpFunction(unsigned char Key, int X, int Y)
{
	//Log("KEY UP %d", Key);
	switch (Key){
	case 27:
		//glutLeaveMainLoop();
		break;
	case 97:		// a
		akey = false;
		break;
	case 100:		// d
		dkey = false;
		break;
	case 43:
		mainCamera.Translate(Vector3(0.0f, 0.0f, -1.0f));
		break;
	case 45:
		mainCamera.Translate(Vector3(0.0f, 0.0f, 1.0f));
		break;
	default:

		break;
	}

	glutPostRedisplay();
}

void SpecialKeyboardDown(int Key, int X, int Y){
	//Log("SKEY DOWN %d", Key);
	switch (Key)
	{
		case GLUT_KEY_UP:
			uparrow = true;
			break;
		case GLUT_KEY_DOWN:
			downArrow = true;
			break;
		case GLUT_KEY_LEFT:
			leftArrow = true;
			break;
		case GLUT_KEY_RIGHT:
			rigtArrow = true;
			break;
		default:
			break;
	}
	glutPostRedisplay();
}

void SpecialKeyboardUp(int Key, int X, int Y){
	//Log("SKEY UP %d", Key);
	switch (Key)
	{
	case GLUT_KEY_UP:
		uparrow = false;
		//mainCamera.Translate(Vector3(0.0f, 0.0f, 0.1f * deltaTime));
		break;
	case GLUT_KEY_DOWN:
		downArrow = false;
		break;
	case GLUT_KEY_LEFT:
		leftArrow = false;
		break;
	case GLUT_KEY_RIGHT:
		rigtArrow = false;
		break;
	default:
		break;
	}
	glutPostRedisplay();
}
